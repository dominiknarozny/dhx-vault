const http = require('http');
const Busboy = require('busboy');
const fs = require('fs');
const express = require('express');
const morgan = require('morgan')
const app = express();
const bodyParser = require('body-parser');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan('\x1b[34m[:date[web]] :method :url :status :response-time ms  \x1b[0m'))

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Origin')
  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'POST, PATCH, PUT, OPTIONS, DELETE, GET')
    return res.status(200).end()
  }
  next()
})

app.use('/upload/files/:file', (req, res) => {
  var file = __dirname + '/files/' + req.params.file;
  res.download(file);
});


app.post('/upload', (req, res) => {
  console.log('foo!')
  // check if the request method is "POST" 
  if (req.method === 'POST') {
    // create a busboy instance for stream parsing of a file
    const busboy = new Busboy({ headers: req.headers });
    const response = {};
    busboy.on('file', (fieldname, file, filename) => {
      // define the directory for a file saving
      const saveTo = __dirname + "/files/" + filename;
      // add the link to this file for response
      response.link = `/upload/files/${filename}`;
      // save the file
      file.pipe(fs.createWriteStream(saveTo));
    });
    busboy.on('finish', () => {
      // return the link to download the file
      res.json(response);
    })
    return req.pipe(busboy);
  }
  // show error 404 in case the request method is other than POST
  res.writeHead(404);
  res.end();
});



const server = http.createServer(app);

server.listen(8000,'localhost', function () {
  console.log(`ITE:TASK API listening`);
});
server.on('error', err => {
  console.log(err)
})